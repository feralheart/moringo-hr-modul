## Installation

- git clone
- cd hrmodule
- composer install
- php artisan key:generate
- insert the database values in the .env file
- php artisan migrate:refresh --seed
- npm install
- php artisan storage:link
- npm dev
- php artisan serve