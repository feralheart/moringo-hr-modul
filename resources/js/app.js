import Vue from 'vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(VueRouter)

import routes from './router.js'

const router = new VueRouter({
    routes
})

import App from './components/App'

const app = new Vue({
    el: '#app',
    components: { 
        App,
    },
    router
})

Vue.config.performance = true