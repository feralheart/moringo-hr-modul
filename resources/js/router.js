import EmployeeList from './components/hr/List'
import EmployeeCreateAndEdit from './components/hr/CreateandEdit'


const routes = [
    {
        path: '/employees',
        component: EmployeeList,
    },
    {
        path: '/employees/:employeeId',
        component: EmployeeCreateAndEdit,
    }
]

export default routes