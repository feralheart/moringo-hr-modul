<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::get('/employees', 'HRController@getEmployeeTable');
    Route::get('/employees/flat', 'HRController@getEmployeeFlat');
    Route::get('/employees/{id}', 'HRController@getEmployee');
    Route::post('/employees', 'HRController@saveEmployee');
    Route::post('/employees/file', 'HRController@addFile');
    Route::delete('/employees', 'HRController@deleteEmployee');
});
