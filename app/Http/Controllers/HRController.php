<?php

namespace App\Http\Controllers;

use App\Models\BankInformation;
use App\Models\EmergencyContact;
use App\Models\Employee;
use App\Models\File;
use Illuminate\Http\Request;

class HRController extends Controller {

    /**
     * @return collection Employee
     */
    public function getEmployeeTable()
    {
        $columns = ['id', 'name', 'email', 'created_at', 'updated_at'];
        return Employee::select($columns)->get();
    }

    /**
     * @return @array
     */
    public function getEmployeeFlat()
    {
        $columns = ['id', 'name'];
        return Employee::select($columns)->get()->toArray();
    }

    /**
     * @param int
     * @return array
     */
    public function getEmployee($id)
    {
        $employee = Employee::find($id);
        $employeePContact = EmergencyContact::where([
            'employee_id' => $id,
            'type' => 'PRIMARY'
        ])->first();
        $employeeSContact = EmergencyContact::where([
            'employee_id' => $id,
            'type' => 'SECONDARY'
        ])->first();
        $employeeBankInfo = BankInformation::where([
            'employee_id' => $id,
        ])->first();
        $files = File::where([
            'employee_id' => $id,
        ])->get();

        return [
            'title' => $employee['title'],
            'name' => $employee['name'],
            'gender' => $employee['gender'],
            'birth_date' => $employee['birth_date'],
            'position' => $employee['position'],
            'department' => $employee['department'],
            'company' => $employee['company'],
            'workhours' => $employee['workhours'],
            'workhour_type' => $employee['workhour_type'],
            'email' => $employee['email'],
            'living_place' => $employee['living_place'],
            'contact_address' => $employee['contact_address'],
            'phone' => $employee['phone'],
            'workphone' => $employee['workphone'],
            'tax_number' => $employee['tax_number'],
            'taj_number' => $employee['taj_number'],
            'nationality' => $employee['nationality'],
            'education' => $employee['education'],
            'languages' => $employee['languages'],
            'superior' => $employee['superior'],
            'tasks_responsiblities' => $employee['tasks_responsiblities'],
            'status' => $employee['status'],

            'primary_contact_name' => $employeePContact['name'],
            'primary_contact_relationship' => $employeePContact['relationship'],
            'primary_contact_phone' => $employeePContact['phone'],

            'secondary_contact_name' => $employeeSContact['name'],
            'secondary_contact_relationship' => $employeeSContact['relationship'],
            'secondary_contact_phone' => $employeeSContact['phone'],

            'bank_name' => $employeeBankInfo['bank_name'],
            'bank_account_number' => $employeeBankInfo['bank_account_number'],
            'files' => $files
        ];
    }

    /**
     * @param Request
     * @return mixed int/bool
     */
    public function saveEmployee(Request $request)
    {
        $saveError = FALSE;
        if($request->id > 0){
            $employeeData = Employee::find($request->id);
            $employeePrimaryEmergencyRaw = EmergencyContact::where([
                'employee_id' => $request->id,
                'type' => 'PRIMARY'
            ])->first();

            $employeePrimaryEmergency = EmergencyContact::find($employeePrimaryEmergencyRaw['id']);

            $employeeSecondaryEmergencyRaw = EmergencyContact::where([
                'employee_id' => $request->id,
                'type' => 'PRIMARY'
            ])->first();

            $employeeSecondaryEmergency = EmergencyContact::find($employeeSecondaryEmergencyRaw['id']);

            $employeeBankInformationRaw = BankInformation::where([
                'employee_id' => $request->id,
            ])->first();

            $employeeBankInformation = BankInformation::find($employeeBankInformationRaw['id']);

            $type = 'update';
        } else {
            $employeeData = new Employee;
            $employeePrimaryEmergency = new EmergencyContact;
            $employeeSecondaryEmergency = new EmergencyContact;
            $employeeBankInformation = new BankInformation;

            $type = 'create';
        }
        $employeeData->name = $request->name;
        $employeeData->title = $request->title;
        $employeeData->gender = $request->input('gender', 'NOINPUT');
        $employeeData->birth_date = $request->birth_date;
        $employeeData->position = $request->position;
        $employeeData->department = $request->department;
        $employeeData->company = $request->company;
        $employeeData->workhours = $request->workhours;
        $employeeData->workhour_type = $request->input('workhour_type', 'MONTHLY');
        $employeeData->email = $request->email;
        $employeeData->living_place = $request->living_place;
        $employeeData->contact_address = $request->contact_address;
        $employeeData->phone = $request->phone;
        $employeeData->workphone = $request->workphone;
        $employeeData->tax_number = $request->tax_number;
        $employeeData->taj_number = $request->taj_number;
        $employeeData->nationality = $request->nationality;
        $employeeData->education = $request->education;
        $employeeData->languages = $request->languages;
        $employeeData->superior = $request->input('superior', 0);
        $employeeData->tasks_responsiblities = $request->tasks_responsiblities;
        $employeeData->status = $request->input('status', true);
        if($employeeData->save()){
            if($type == 'create'){
                $employeePrimaryEmergency->employee_id = $employeeData->id;
                $employeePrimaryEmergency->type = 'PRIMARY';

                $employeeSecondaryEmergency->employee_id = $employeeData->id;
                $employeeSecondaryEmergency->type = 'SECONDARY';

                $employeeBankInformation->employee_id = $employeeData->id;
            }

            $employeePrimaryEmergency->name = $request->primary_contact_name;
            $employeePrimaryEmergency->relationship = $request->primary_contact_relationship;
            $employeePrimaryEmergency->phone = $request->primary_contact_phone;
            if(!$employeePrimaryEmergency->save()){
                $saveError = TRUE;
            } 

            $employeeSecondaryEmergency->name = $request->secondary_contact_name;
            $employeeSecondaryEmergency->relationship = $request->secondary_contact_relationship;
            $employeeSecondaryEmergency->phone = $request->secondary_contact_phone;
            if(!$employeeSecondaryEmergency->save()){
                $saveError = TRUE;
            }

            $employeeBankInformation->bank_name = $request->bank_name;
            $employeeBankInformation->bank_account_number = $request->bank_account_number;
            if(!$employeeBankInformation->save()){
                $saveError = TRUE;
            }

            if($saveError == FALSE){
                return $employeeData->id;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param Request
     * @return string
     */
    public function deleteEmployee(Request $request)
    {
        if(Employee::find($request->id)->delete()){
            File::where('employee_id', $request->id)->delete();
            return 'success';
        } else {
            return 'error';
        }
    }

    /**
     * @param int
     * @return array
     */
    public function getEmployeeFiles($employeeId)
    {
        $fileArray = [];
        $employeeFiles = Employee::find($employeeId)->files();
        foreach($employeeFiles as $file){
            $employeeFiles[] = [
                'name' => $file['name'],
                'path' => $file['path'],
            ];
        }

        return $fileArray;
    }

    /**
     * @param Request
     * @param int
     * @return mixed
     */
    public function addFile(Request $request)
    {
        $employeeId = $request->id;
        $fileName = $employeeId.'-'.$request->file('uploadedFile')->getClientOriginalName();

        $path = 'storage/'.$request->file('uploadedFile')->storeAs('', $fileName, 'public');

        $saveFile = new File;
        $saveFile->employee_id = $employeeId;
        $saveFile->name = $fileName;
        $saveFile->path = asset($path);
        $saveFile->save();

        return $saveFile->id;
    }
}
