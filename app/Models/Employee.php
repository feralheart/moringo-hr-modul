<?php

namespace App\Models;

use App\Models\BankInformation;
use App\Models\EmergencyContact;
use App\Models\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Employee extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'gender', 'birth_date', 'position', 'department', 'company', 'workhours', 'workhour_type', 'email', 'living_place', 'contact_address', 'phone', 'company_phone', 'tax_number', 'taj_number', 'nationality', 'study', 'languages', 'superior', 'tasks_responsibilities', 'status'];

    /**
     * The relations what will be cascade deleted
     * 
     * @var array
     */
    protected $softCascade = ['emergencyContact', 'bankInformation'];

    /**
     * Relations
     */
    public function emergencyContact()
    {
        return $this->hasMany(EmergencyContact::class);
    }

    public function bankInformation()
    {
        return $this->hasone(BankInformation::class);
    }

    public function file()
    {
        return $this->hasone(File::class);
    }
}
