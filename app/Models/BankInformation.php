<?php

namespace App\Models;

use App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankInformation extends Model
{
    use SoftDeletes;

    /**
     * The table what should be updated on update event
     * 
     * @var array
     */
    protected $thouches = ['employee'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bank_informations';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['employee_id', 'bank_name', 'bank_account_number'];

    /**
     * Relations
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
