<?php

namespace App\Models;

use App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The table what should be updated on update event
     * 
     * @var array
     */
    protected $thouches = ['employee'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['employee_id', 'name', 'path'];

    /**
     * Relations
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
