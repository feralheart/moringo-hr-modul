<?php

namespace App\Models;

use App\Models\Employee;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmergencyContact extends Model
{
    use SoftDeletes;

    /**
     * The table what should be updated on update event
     * 
     * @var array
     */
    protected $thouches = ['employee'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'emergency_contacts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['employee_id', 'type', 'relationship', 'phone'];

    /**
     * Relations
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
