<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('gender')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('position')->nullable();
            $table->string('department')->nullable();
            $table->string('company')->nullable();
            $table->unsignedInteger('workhours')->nullable();
            $table->string('workhour_type')->nullable();
            $table->string('email')->nullable();
            $table->string('living_place')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('workphone')->nullable();
            $table->string('tax_number')->nullable();
            $table->string('taj_number')->nullable();
            $table->string('nationality')->nullable();
            $table->string('education')->nullable();
            $table->string('languages')->nullable();
            $table->unsignedInteger('superior')->nullable();
            $table->longText('tasks_responsiblities')->nullable();
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('emergency_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('employee_id');
            $table->string('type');
            $table->string('name')->nullable();
            $table->string('relationship')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bank_informations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('employee_id');
            $table->string('bank_name')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('employee_id');
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
        Schema::dropIfExists('emergency_contacts');
        Schema::dropIfExists('bank_informations');
        Schema::dropIfExists('files');
    }
}
