<?php

use App\Http\Controllers\HRController;
use Illuminate\Database\Seeder;

use Illuminate\Http\Request;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $hrController = new HRController;
        $hrController->saveEmployee(new Request([
            'id' => 0,
            'name' => 'Toby Fox',
            'status' => 1
        ]));
    }
}
